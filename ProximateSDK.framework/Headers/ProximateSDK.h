//
//  ProximateSDK.h
//  ProximateSDK
//
//  Created by Afzaal Ahmad on 02/03/2017.
//  Copyright © 2017 DevCrew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ESTBeaconManager.h"
#import "EstimoteSDK.h"

//! Project version number for ProximateSDK.
FOUNDATION_EXPORT double ProximateSDKVersionNumber;

//! Project version string for ProximateSDK.
FOUNDATION_EXPORT const unsigned char ProximateSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ProximateSDK/PublicHeader.h>


